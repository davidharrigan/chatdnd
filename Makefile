VERSION=$(shell git describe --tags 2> /dev/null || echo '0.0.0')
PYTHON=./venv/bin/python3
PID=dnd.pid

run:
	$(PYTHON) dnd.py

start:
	$(PYTHON) dnd.py > /dev/null 2>&1 & echo $$! > $(PID)

kill:
	kill $$(cat $(PID))

